---
title: "Job Interviews and the Dreaded Technical Test"
tags: ["slack", "monster", "indeed"]
published: true
date: "2021-01-18"
---

I got laid off from my Software Engineer position in October, 2020, thanks to COVID wreaking havoc on the industry in which I was working (travel). Over the next three and a half months, I submitted more than 60 applications for remote positions (mainly Software Engineer, Senior Software Engineer, Full Stack Software Engineer, or some variation). The market for remote workers is wide open right now, since that's what most companies are looking for, and that's awesome! But what isn't awesome is: I was no longer just competing against job seekers in my own geographic market; I was competing with, well, the entire country.
<br><br>
Of my applications, I managed to get first interviews on a little less than half of them. Of those first interviews, I was invited to do some form of skills test or evaluation on maybe half of them. I have the exact number of skills tests that I did, along with notes and files for each one, but I'm too lazy to look it up right now; so I'm going to guess that I've done a dozen of them.
<br><br>
Technical evaluations may be any combination of actual tests (timed, like the kind you took in school), "take home" projects that you can work on and submit, or (my least favorite) the dreaded live session where one or more software engineers watches you try to write code on command. Despite having done this type of evaluation numerous times, I completely suck at it. I have the same type of performance anxiety that one may experience when the lab tech thrusts a plastic cup in your hand and orders you to fill it to the line with pee. All of my life, I have been an excellent pee-er; peeing just comes naturally to me. But when I'm told to do it, my peeing instinct just vanishes. Same with coding on command.
<br><br>
My advice to any devs who are going through the job hunting process in this new post-Corona working landscape is: be persistent! There are no shortage of openings for you to apply to, but just be prepared to apply to a LOT of them. I lost track of how many of my interviews felt super positive to me... but then I'd get an email saying that they had chosen another candidate (probably someone who's better at peeing on command than I am.)
<br><br>
Consider each interview and evaluation to be a new chance for you to improve on how you've done on previous ones. And if/when you get a "thanks but no thanks" from a potential employer, see if they gave you any feedback on WHY you weren't chosen or what you could have improved. Most of them won't do that unless you ask, so ask! Getting feedback is crucial to improving, and I found that most of the people whom I reached out to for feedback were more than happy to give me some tips.
