import React from "react"
import {
  FaLinkedin,
  FaGithubSquare,
  FaGitlab,
  FaCampground,
  FaFacebookF,
} from "react-icons/fa"
import "./sidebar.css"

const SocialLinks = ({ contacts }) => {
  return (
    <div className="side-social-links float-left mt-3 mb-3">
      <a className="text-secondary p-2" href={contacts.linkedin}>
        <span title="Linked In">
          <FaLinkedin size={26} style={{ color: "secondary" }} />
        </span>
      </a>
      <a className="text-secondary p-2" href={contacts.github}>
        <span title="GitHub">
          <FaGithubSquare size={26} style={{ color: "secondary" }} />
        </span>
      </a>
      <a className="text-secondary p-2" href={contacts.gitlab}>
        <span title="Gitlab">
          <FaGitlab size={26} style={{ color: "secondary" }} />
        </span>
      </a>
      <a className="text-secondary p-2" href={contacts.nucamp}>
        <span title="Nucamp">
          <FaCampground size={26} style={{ color: "secondary" }} />
        </span>
      </a>
      <a className="text-secondary p-2" href={contacts.facebook}>
        <span title="Facebook">
          <FaFacebookF size={26} style={{ color: "secondary" }} />
        </span>
      </a>
    </div>
  )
}

export default SocialLinks
